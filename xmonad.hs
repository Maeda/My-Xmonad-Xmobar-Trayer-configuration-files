--Divers
import XMonad
import XMonad.Config.Azerty
import Control.Monad (liftM2)
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeysP)
import qualified XMonad.StackSet as W
import System.IO
import XMonad.Util.NamedScratchpad
import System.Environment
import System.FilePath.Posix
import XMonad.Prompt
import XMonad.Prompt.Pass
import XMonad.Prompt.AppendFile
import XMonad.Prompt.FuzzyMatch
import System.Directory
import System.FilePath.Find
import XMonad.Util.SpawnOnce

--Hooks
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers as H
import XMonad.Hooks.InsertPosition
import XMonad.Hooks.UrgencyHook

--Layouts
import XMonad.Layout.NoBorders
import XMonad.Layout.IM
import XMonad.Layout.PerWorkspace
import XMonad.Layout.Spacing
import XMonad.Layout.Grid
import XMonad.Layout.Renamed
import XMonad.Layout.Reflect

--IM
import Data.Ratio ((%))

--Terminal préféré
myTerminal = "urxvt"

--Raccourcis
myKeys =
    -- Multimedia Keys
        [ ("<XF86AudioPlay>", spawn "if [ ! $(pidof -s audacious) ];then audacious;fi;amixer -c PAD cset name='Attenuation' 3;audtool --playback-playpause")
        --, ("C-<XF86AudioLowerVolume>", spawn "ssh rpi3 'source .bashrc;audtool --playlist-reverse'")")
        , ("C-<XF86AudioLowerVolume>", spawn "audtool --playlist-reverse")
        , ("C-<XF86AudioRaiseVolume>", spawn "audtool --playlist-advance")
        --, ("<XF86AudioPlay>", spawn "ssh rpi3 'source .bashrc;if [ ! $(pidof -s audacious) ];then `audacious -H &>/dev/null & sleep 1`;fi;audtool --playback-playpause'")
        --, ("C-<XF86AudioLowerVolume>", spawn "ssh rpi3 'source .bashrc;audtool --playlist-reverse'")
        --, ("C-<XF86AudioRaiseVolume>", spawn "ssh rpi3 'source .bashrc;audtool --playlist-advance'")
        --, ("<XF86AudioMute>", spawn "amixer -c PAD cset name='Attenuation' 3;amixer sset DAC 1%")
        , ("<XF86AudioMute>", spawn "amixer -c Balanced sset 'D10 Balanced' toggle")
        --, ("<XF86AudioLowerVolume>", spawn "amixer -c PAD cset name='Attenuation' 3;amixer sset DAC 5%-")
        , ("<XF86AudioLowerVolume>", spawn "amixer -c Balanced sset 'D10 Balanced' 1%- -M")
        --, ("<XF86AudioRaiseVolume>", spawn "amixer -c PAD cset name='Attenuation' 3;amixer sset DAC 5%+")
        , ("<XF86AudioRaiseVolume>", spawn "if [ $(amixer -c Balanced | grep -Eo '[0-9]%|[0-9][0-9]%|0%|100%' | cut -d '%' -f1) -lt 79 ];then amixer -c Balanced sset 'D10 Balanced' 1%+ -M;fi")
        --, ("S-<XF86AudioLowerVolume>", spawn "amixer -c PAD cset name='Attenuation' 3;amixer sset DAC 1%-")
        --, ("S-<XF86AudioLowerVolume>", spawn "amixer -c Balanced sset 'D10 Balanced' 1%- -M")
        --, ("S-<XF86AudioRaiseVolume>", spawn "amixer -c PAD cset name='Attenuation' 3;amixer sset DAC 1%+")
        --, ("S-<XF86AudioRaiseVolume>", spawn "amixer -c Balanced sset 'D10 Balanced' 1%+ -M")
        , ("<XF86HomePage>", spawn "firefox")
        , ("<XF86Search>", spawn "nautilus")
        , ("<XF86Favorites>", spawn "")
        , ("<XF86Mail>", spawn "thunderbird")
        , ("<XF86Calculator>", spawn "gnome-calculator")
        , ("<XF86Tools>", spawn "~/Documents/Scripts/Système/Audacious/ssv_ziks1")
        , ("C-<XF86Tools>", spawn "audtool --shutdown")
        --, ("C-<XF86Tools>", spawn "ssh rpi3 'source .bashrc;audtool --shutdown'")
        , ("<XF86Explorer>", spawn "~/Documents/Scripts/Vidéos/tv/Programme_tv")
        , ("C-<XF86Explorer>", spawn "~/Documents/Scripts/Vidéos/tv/tv")
        , ("<Print>", spawn "sleep 0.2; scrot -s '%Y%m%d%H%M%S.png' -e 'mv $f ~/Images/Captures/'")
        , ("C-<Print>", spawn "scrot '%Y%m%d%H%M%S.png' -e 'mv $f ~/Images/Captures/'")
        , ("<XF86AudioStop>", spawn "audtool --playback-stop")
        , ("<XF86AudioPrev>", spawn "audtool --playlist-reverse")
        , ("C-<XF86AudioPrev>", spawn "audtool --set-current-playlist 6 --select-playing")
        , ("<XF86AudioNext>", spawn "audtool --playlist-advance")
        , ("C-<XF86AudioNext>", spawn "audtool --set-current-playlist 9 --select-playing")
        --, ("<XF86AudioStop>", spawn "ssh rpi3 'source .bashrc;audtool --playback-stop'")
        --, ("<XF86AudioPrev>", spawn "ssh rpi3 'source .bashrc;audtool --playlist-reverse';~/Documents/Scripts/Système/Audacious/ssv_ziks1")
        --, ("C-<XF86AudioPrev>", spawn "ssh rpi3 'source .bashrc;audtool --set-current-playlist 6 --select-playing';~/Documents/Scripts/Système/Audacious/ssv_ziks1")
        --, ("<XF86AudioNext>", spawn "ssh rpi3 'source .bashrc;audtool --playlist-advance';~/Documents/Scripts/Système/Audacious/ssv_ziks1")
        --, ("C-<XF86AudioNext>", spawn "ssh rpi3 'source .bashrc;audtool --set-current-playlist 9 --select-playing';~/Documents/Scripts/Système/Audacious/ssv_ziks1")
        , ("<XF86Documents>", spawn "nautilus --no-desktop")
        --, ("C-<Pause>", spawn "gksudo poweroff")
        , ("<XF86Sleep>", spawn "gksudo poweroff")
        , ("M-p", spawn "gmrun")
        , ("M-g", spawn "gramps")
        , ("M-s", spawn "shotwell")
        , ("M-w", namedScratchpadAction scratchpads "term")
        --, ("<XF86Eject>", spawn "~/Documents/Scripts/Système/Audacious/ssv_ziks1")
        , ("M-m", passPrompt myXPConfig)
--        , (("M-n"), appendFilePrompt myXPConfig "NOTES")
        , ("M-n", namedScratchpadAction scratchpads "note")
        , ("M-a", spawn "zenity --info --title 'Aide' --text \"$(cat ~/.xmonad/HELP)\" --width=1200 --height=600")
        , ("C-<Pause>", spawn "( slock && xset dpms 0 0 0 ) & xset dpms 0 0 12;xset dpms force off")
        ]

--Workspaces
myWorkspaces = ["1", "2M", "3w", "4Z", "5F", "6B", "7P", "8o", "9C"]

--Hooks
myLayout = onWorkspace "2M" tiersLayout $ onWorkspace "3w" quarterLayout $ onWorkspace "6B" quarterLayout $ onWorkspace "7P" gimpLayout $ onWorkspace "9C" imLayout $ standardLayouts
  where
   standardLayouts = avoidStruts $ smartBorders $ (horiz ||| verti ||| full)
   verti = renamed [Replace "[V]"] $ Tall nmaster delta ratio
   nmaster = 1
   ratio = 1/2
   delta = 3/100
   horiz = renamed [Replace "[H]"] $ Mirror verti
   full = renamed [Replace "[F]"] $ Full
   imLayout = avoidStruts $ smartBorders $ (renamed [Replace "[IM]"] $ withIM (1/5) (Role "roster") gridLayout) ||| standardLayouts
   gridLayout = Grid 
   tiersLayout = avoidStruts $ smartBorders $ (tiersH ||| tiersV ||| full)
    where
     tiersV = renamed [Replace "[V3]"] $ Tall nmastertiers deltatiers ratiotiers
     nmastertiers = 1
     deltatiers = 3/100
     ratiotiers = 66/100
     tiersH = renamed [Replace "[H3]"] $ Mirror tiersV
   quarterLayout = avoidStruts $ smartBorders $ (quarterH ||| quarterV ||| full)
    where
     quarterV = renamed [Replace "[V4]"] $ Tall nmasterquarter deltaquarter ratioquarter
     nmasterquarter = 1
     deltaquarter = 3/100
     ratioquarter = 75/100
     quarterH = renamed [Replace "[H4]"] $ Mirror quarterV
   -- zikLayout = avoidStruts $ (zik ||| Mirror zik ||| Full)
    -- where
    -- zik = Tall nmasterzik deltazik ratiozik
    -- nmasterzik = 2
    -- deltazik = 3/100
    -- ratiozik = 66/100
   gimpLayout = avoidStruts $ smartBorders $ (renamed [Replace "[Gimp]"] $ withIM 0.10 (Role "gimp-toolbox-1") $ reflectHoriz $ withIM 0.19 (Role "gimp-dock-1") gridLayout) ||| horiz ||| verti

myManageHook = (composeAll
   [ className =? "Rhythmbox" --> doShift "4Z"
   , resource =? "vdpau" --> doFloat
   , resource =? "gimp" --> doShift "7P"
   , className =? "openshot" --> doShift "7P"
   , className =? "thunderbird" --> doShift "2M"
   , role =? "Msgcompose" --> doFloat
   , role =? "Preferences" --> doFloat
   , role =? "filterlist" --> doFloat
   , role =? "EventDialog" --> doFloat
   , role =? "certmanager" --> doFloat
   , role =? "AlarmWindow" --> doFloat
   , className =? "Xmessage"  --> doFloat
   , className =? "firefox" --> doShift "3w"
   , resource =? "boincmgr" --> doShift "6B"
   , className =? "F-spot" --> doShift "7P"
   , className =? "Shotwell" --> doShift "7P"
   , className =? "zenity" --> doFloat
   , className =? "Empathy" --> doShift "9C"
   , className =? "Gajim" --> doShift "9C"
   --A-- and float everything but the roster
   --A-- , classNotRole ("Gajim", "roster") --> doFloat
   , className =? "Mumble" --> doShift "9C"
   , className =? "discord" --> doShift "9C"
   , className =? "Bombono-dvd" --> doShift "5F"
   , className =? "Devede" --> doShift "5F"
   , className =? "Photivo" --> doShift "7P"
   , className =? "Rawtherapee" --> doShift "7P"
   , className =? "Remmina" --> doShift "1"
   , className =? "VirtualBox" --> doShift "8o"
   , className =? "Cinelerra" --> doShift "5F"
   , className =? "Avidemux" --> doShift "5F"
   , className =? "Ghb" --> doShift "5F"
   , className =? "Vmplayer" --> doShift "8o"
   , className =? "Freetuxtv" --> doShift "5F"
   , className =? "Audacious" --> doShift "4Z"
   , className =? "Inkscape" --> doShift "7P"
   , resource =? "canto-xmonad" --> doShift "3w"
   , resource =? "irssi-xmonad" --> doShift "9C"
   , resource =? "file_properties" --> doFloat
   , resource =? "gramps" --> doShift "8o"
   , (className H./=? "Audacious" <&&> fmap not isDialog <&&> role H./=? "Msgcompose" <&&> role H./=? "Preferences" <&&> role H./=? "filterlist" <&&> role H./=? "EventDialog" <&&> role H./=? "certmanager" <&&> role H./=? "AlarmWindow") --> insertPosition Below Newer
   , resource =? "feh" --> doFloat
   , resource =? "FAHControl" --> doShift "6B"
   , className =? "Onboard" --> doFloat
   , className =? "Onboard" --> doIgnore
   , resource =? "gnome-calculator" --> doFloat
   , resource =? "qemu-system-x86_64" --> doShift "8o"
   , resource =? "gmrun" --> doFloat
   , manageDocks
   ])
   where
     --A-- classNotRole :: (String, String) -> Query Bool
     --A-- classNotRole (c,r) = className =? c <&&> role /=? r
    role = stringProperty "WM_WINDOW_ROLE"

scratchpads = [
    NS "term" "urxvt -name scratchpad -pe tabbed" (resource =? "scratchpad")
        (customFloating $ (W.RationalRect (1/100) (72/100) (98/100) (25/100))),
    NS "note" "urxvt -name note -pe tabbed -e vim ~/Notes/NOTES" (resource =? "note")
        (customFloating $ (W.RationalRect (60/100) (25/100) (39/100) (40/100)))
  ]

myStartupHook = do
--dans le crontab root!    spawnOnce "alsactl monitor | while read Playback; do if [ $(amixer -c Balanced | grep -Eo '[8-9][0-9]%|100%' | cut -d '%' -f1) ];then amixer -c Balanced sset 'D10 Balanced' mute 49%;fi;done"
    spawnOnce "systemctl --user stop pulseaudio.socket"
    spawnOnce "systemctl --user disable pulseaudio.service"
    spawnOnce "xmobar ~/.xmobar/xmobarrc2.hs"
    spawnOnce "zenity --info --title 'Aide' --text \"$(cat ~/.xmonad/HELP)\" --width=1200 --height=600"
    spawnOnce "redshift -l 45.6457:5.7602"
    spawnOnce "boincmgr -n ryzinc"
    spawnOnce "parcellite"
    spawnOnce "numlockx"
    spawnOnce "firefox"
    spawnOnce "thunderbird"
    spawnOnce "urxvt -name 'canto-xmonad' -e canto-curses"
    spawnOnce "~/Documents/Scripts/Système/Fond_écran/./Fond_écran 360 1800 10"
    spawnOnce "~/Documents/Scripts/Système/Thèmes/./saisons"
    spawnOnce "xset dpms 0 0 0"
    spawnOnce "xset s 0 0"
--    spawnOnce "urxvt -name 'irssi-xmonad' -e irssi"

--Variables persos
----THEMES coutour fenêtres /!\ géré en auto par script saisons
myTheme = "#A52A2A" --Thème Automne
--myTheme = "#000080" --Thème Été
--myTheme = "#009698" --Thème Hiver
--myTheme = "#00CC3A" --Thème Noel
--myTheme = "#FFD700" --Thème Printemps

----THEMES fond fenêtres/!\ géré en auto par script saisons
mybgColor = "#FFAC4B" --Thème Automne
--mybgColor = "#146565" --Thème Été
--mybgColor = "#F8F8FF" --Thème Hiver
--mybgColor = "#B21900" --Thème Noel
--mybgColor = "#205E27" --Thème Printemps

----THEMES couleur texte/!\ géré en auto par script saisons
myfgColor = "black" --Thème Automne
--myfgColor = "#FDFF7F" --Thème Été
--myfgColor = "#333333" --Thème Hiver
--myfgColor = "white" --Thème Noel
--myfgColor = "#EEE8AA" --Thème Printemps

myXPConfig = def
                   { position = Bottom
                   , promptBorderWidth = 1
                   , font = "xft:DejaVu Sans:size=15:bold:antialias=true"
                   , borderColor = myTheme
                   , fgColor = myfgColor
                   , bgColor = mybgColor
                   , height = 25
                   , searchPredicate = fuzzyMatch
                   , sorter          = fuzzySort
                   }

main = do
    xmproc <- spawnPipe "xmobar ~/.xmobar/xmobarrc.hs"
    xmonad $ withUrgencyHook NoUrgencyHook . docks $ azertyConfig
        { workspaces = myWorkspaces
        , terminal = myTerminal
        , manageHook = myManageHook <+> manageHook def <+> namedScratchpadManageHook scratchpads
        , layoutHook = myLayout
        , logHook = dynamicLogWithPP xmobarPP
                        { ppOutput = hPutStrLn xmproc
                        , ppTitle = xmobarColor myTheme "" . shorten 50
                        , ppCurrent = xmobarColor myTheme "" . wrap "{" "}"
                        }
        , startupHook = myStartupHook
        , modMask = mod4Mask     -- Touche Super
        , focusedBorderColor = myTheme -- Couleur coutour fenêtre qui a focus
        , borderWidth = 4
        } `additionalKeysP` myKeys

