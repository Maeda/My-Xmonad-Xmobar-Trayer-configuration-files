Config { font = "DejaVu Sans Bold 14" --font = "xft:DejaVu Sans:size=18:bold:antialias=true" --font = "DejaVu Sans Bold, Noto Color Emoji 18"
--Icon folder
       , iconRoot = "Images/Icônes/"
--Season theme : background
--       , bgColor = "#FFAC4B" --Thème Automne
--       , bgColor = "#146A65" --Thème Été
--       , bgColor = "#F8F8FF" --Thème Hiver
--       , bgColor = "#B21900" --Thème Noel
       , bgColor = "#205e27" --Thème Printemps
--Season theme : foreground
--       , fgColor = "black" --Thème Automne
--       , fgColor = "#FDFF7F" --Thème Été
--       , fgColor = "#333333" --Thème Hiver
--       , fgColor = "white" --Thème Noel
       , fgColor = "#EEE8AA" --Thème Printemps
       , persistent = True
       , hideOnStart = False
       , alpha = 175
       , position = BottomH 24
--       , lowerOnStart = True
       , commands = [
                    Run Com "/bin/bash" ["-c","date '+%a %d %b %R'"] "date" 600
                    --Audacious music player
                    , Run Com "/bin/bash" ["-c","if [ $(pidof audacious) ];then audtool --current-song;else echo -;fi"] "aud1" 30
--                    , Run Com "/bin/bash" ["-c","if [ $(pidof audacious) ];then audtool --current-song-output-length;else echo -;fi"] "aud2" 50
                    , Run Com "/bin/bash" ["-c","if [ $(pidof audacious) ];then ~/Documents/Scripts/Système/Audacious/xmobar-status;fi"] "aud2" 50
--                    , Run Com "/bin/bash" ["-c","[ $(nc -z rpi3 25852;echo $?) -eq 0 ] && ~/Documents/Scripts/Système/Audacious/xmobar-status"] "aud2" 50
--                    , Run Com "/bin/bash" ["-c","echo `curl http://fr.wttr.in/La-Chapelle-Saint-Martin?format=2` | sed 's/[🌡️🌬️°Ckm/h]//g' | sed 's/⛅/◌/g' | sed 's/[⛈🌩]/⚡/g' | sed 's/[🌦🌧]/░/g' | sed 's/[🌫]/∭/g' | sed 's/[🌨]/☃/g'"] "met" 6000
                    , Run Alsa "hw:0" "DAC" ["-t","<volume>%","-L","15","-H","50","--normal","green","--high","red"]
--                    , Run Weather "LFLB" ["-t","<windKmh><windCardinal> <tempC>°<skyCondition>"] 36000
                    , Run WeatherX "LFLB" [("clear", "☀"),("sunny", "☀"),("mostly clear", "☀"),("mostly sunny", "☀"),("partly sunny", "🌤"),("fair", "🌑"),("cloudy","☁"),("overcast","☁"),("partly cloudy", "☁"),("mostly cloudy", "☁"),("considerable cloudiness", "☁")] ["-t","<windKmh><windCardinal> <tempC>°<fn=2><skyConditionS></fn>"] 36000
                    ]
       , sepChar = "%"
       , alignSep = "}{"
--       , template = "<action=`~/Documents/Scripts/Système/Audacious/ssv_ziks1` button=1><action=`~/Documents/Scripts/Système/Audacious/track_details` button=3><icon=music_icon.xpm/></action></action><action=`audtool --playlist-advance` button=1><action=`audtool --playlist-reverse` button=3>%aud1% </action></action><action=`audtool --playback-playpause` button=1>%aud2%</action>}{%alsa:hw:0:DAC% | %LFLB% | <action=`zenity --calendar` button=1><action=`onboard` button=3><fc=#A52A2A>%date%</fc></action></action>" --Thème Automne
--       , template = "<action=`~/Documents/Scripts/Système/Audacious/ssv_ziks1` button=1><action=`~/Documents/Scripts/Système/Audacious/track_details` button=3><icon=music_icon.xpm/></action></action><action=`audtool --playlist-advance` button=1><action=`audtool --playlist-reverse` button=3>%aud1% </action></action><action=`audtool --playback-playpause` button=1>%aud2%</action>}{%alsa:hw:0:DAC% | %LFLB% | <action=`zenity --calendar` button=1><action=`onboard` button=3><fc=#000080>%date%</fc></action></action>" --Thème Été
--       , template = "<action=`~/Documents/Scripts/Système/Audacious/ssv_ziks1` button=1><action=`~/Documents/Scripts/Système/Audacious/track_details` button=3><icon=music_icon.xpm/></action></action><action=`audtool --playlist-advance` button=1><action=`audtool --playlist-reverse` button=3>%aud1% </action></action><action=`audtool --playback-playpause` button=1>%aud2%</action>}{%alsa:hw:0:DAC% | %LFLB% | <action=`zenity --calendar` button=1><action=`onboard` button=3><fc=#009698>%date%</fc></action></action>" --Thème Hiver
--       , template = "<action=`~/Documents/Scripts/Système/Audacious/ssv_ziks1` button=1><action=`~/Documents/Scripts/Système/Audacious/track_details` button=3><icon=music_icon.xpm/></action></action><action=`audtool --playlist-advance` button=1><action=`audtool --playlist-reverse` button=3>%aud1% </action></action><action=`audtool --playback-playpause` button=1>%aud2%</action>}{%alsa:hw:0:DAC% | %LFLB% | <action=`zenity --calendar` button=1><action=`onboard` button=3><fc=#000000>%date%</fc></action></action>" --Thème Noel
       , template = "<action=`~/Documents/Scripts/Système/Audacious/ssv_ziks1` button=1><action=`~/Documents/Scripts/Système/Audacious/track_details` button=3><icon=music_icon.xpm/></action></action><action=`audtool --playlist-advance` button=1><action=`audtool --playlist-reverse` button=3>%aud1% </action></action><action=`audtool --playback-playpause` button=1>%aud2%</action>}{%alsa:hw:0:DAC% | %LFLB% | <action=`zenity --calendar` button=1><action=`onboard` button=3><fc=#FFD700>%date%</fc></action></action>" --Thème Printemps
       }
